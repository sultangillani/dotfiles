// Base16 Synth Midnight Dark
// Scheme: Michaël Ball (http://github.com/michael-ball/)

base00 = '#040404';
base01 = '#141414';
base02 = '#242424';
base03 = '#61507A';
base04 = '#BFBBBF';
base05 = '#DFDBDF';
base06 = '#EFEBEF';
base07 = '#FFFBFF';
base08 = '#B53B50';
base09 = '#E4600E';
base0A = '#DAE84D';
base0B = '#06EA61';
base0C = '#7CEDE9';
base0D = '#03AEFF';
base0E = '#EA5CE2';
base0F = '#9D4D0E';

t.prefs_.set('color-palette-overrides', 
                        [base00,
                        base08,
                        base0B,
                        base0A,
                        base0D,
                        base0E,
                        base0C,
                        base05,
                        base03,
                        base08,
                        base0B,
                        base0A,
                        base0D,
                        base0E,
                        base0C,
                        base07,
                        base09,
                        base0F,
                        base01,
                        base02,
                        base04,
                        base06]);

t.prefs_.set('cursor-color', "rgba(223, 219, 223, 0.5)");
t.prefs_.set('foreground-color', base05);
t.prefs_.set('background-color', base00);
