---
- name: read vars from first file found, use 'vars/' relative subdir
  include_vars: "{{ lookup('first_found', params) }}"
  vars:
    params:
      files:
        - '{{ ansible_facts["os_family"]|lower }}.yml'
        - default.yml
      paths:
        - 'vars'

- name: include os specific tasks
  include_tasks: "{{ ansible_facts['os_family']|lower }}.yml"
  when: "ansible_facts['os_family']|lower is defined"

- name: Create the list of ruby versions.
  set_fact:
    rbenv_ruby_versions: "default_ruby"
  tags:
    - initial

- name: set rbenv_owner
  set_fact:
    rbenv_owner: "{{ zsh_user }}"
  tags:
    - initial

- name: set tmp directory path
  set_fact:
    rbenv_tmpdir: "{{ ansible_env.TMPDIR | default('/tmp') }}"
  when: rbenv_tmpdir is undefined
  tags:
    - initial

- name: create plugins directory for selected users
  file:
    state: directory
    path: "{{ rbenv_root }}/plugins"
  tags:
    - initial

- name: install plugins for selected users
  git:
    repo: "{{ plug['repo'] }}"
    dest: "{{ rbenv_root }}/plugins/{{ plug['name'] }}"
    update: no
    version: master
  loop: "{{ rbenv_plugins }}"
  loop_control:
    loop_var: plug
  notify: update rbenv
  tags:
    - initial

- name: set default-gems for select users
  copy:
    src: default-gems
    dest: "{{ rbenv_root }}/default-gems"
  tags:
    - update
    - gem

- name: check ruby versions installed for select users
  command: "{{ rbenv_shell }} versions --bare"
  register: rbenv_versions
  changed_when: no
  failed_when: no
  tags:
    - initial

- name: install rubies {{ rbenv.rubies }}
  command: "{{ rbenv_shell | quote }} install --skip-existing {{ rubv | quote }} --verbose"
  args:
    creates: "{{ dotfiles_user_home }}/.rbenv/versions/{{ rubv }}"
  loop: "{{ rbenv['rubies'] }}"
  loop_control:
    loop_var: rubv
  tags:
    - initial

- name: check if ruby version is {{ rbenv['default_ruby'] }}
  shell: |
    set -o pipefail
    "{{ rbenv_shell }} version | cut -d ' ' -f 1 | grep -Fx '{{ rbenv['default_ruby'] }}'"
  register: ruby_selected
  changed_when: no
  failed_when: no
  check_mode: no
  tags:
    - initial

- name: set ruby {{ rbenv.default_ruby }}
  command: "{{ rbenv_shell }} global {{ rbenv['default_ruby'] }}"
  notify: rehash rbenv
  changed_when: no
  tags:
    - initial
