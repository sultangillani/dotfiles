##############################################################
# Directory navigation options
###############################################################

setopt AUTO_CD

setopt AUTO_PUSHD

setopt PUSHD_IGNORE_DUPS

setopt PUSHD_SILENT

setopt PUSHD_TO_HOME

setopt AUTOPARAMSLASH

### Globbing and fds ###

setopt EXTENDED_GLOB

setopt MULTIOS

setopt NO_CLOBBER
